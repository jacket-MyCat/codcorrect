CoDCorrect
=====

### Overview ###
CoDCorrect is a core piece of mortality machinery that is primarily responsible for making the Causes of Death and All-cause Mortality results consistent within the Global Burden of Disease (GBD) Study. Sanitized code for the 2019 round of the GBD Study can be found here:

https://github.com/ihmeuw/ihme-modeling/tree/master/gbd_2019/shared_code/central_comp/cod/codcorrect/codcorrect

Many developers contributed to this codebase. My contribution was to refactor and merge the CoDCorrect code with a newly designed piece of research machinery called FauxCorrect. FauxCorrect is still in active development.

### More Details ###
Using the CoDCorrect cause hierarchy, the CoDCorrect machinery reads in draw files from Causes of Death models (CODEm and custom models) and scales all of the level 1 CoD model data to the all-cause mortality envelope. In other words, after CoDCorrect is run, all of the level 1 causes will add up to the all-cause mortality envelope.

CoDCorrect then flows down the CoDCorrect hierarchy and scales the sub-causes of the level 1 causes to match the parent cause. The result is that all sub-causes add up to their parent cause and all CoD data adds up to the all-cause mortality envelope.
